import unittest
from main import get_playlist


class GetPlaylistTest(unittest.TestCase):

    def test_artists_name(self):
        playlist = get_playlist("unitest/test.json")
        artists = ["eminem", "nirvana"]
        res = True
        for music in playlist:
            if not(music["artist"] in artists):
                res = False
        self.assertTrue(res)


if __name__ == '__main__':
    unittest.main()
