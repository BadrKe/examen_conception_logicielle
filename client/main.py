from asyncore import write
import random
import requests
import json
from dotenv import load_dotenv
from dotenv import dotenv_values
import os


def get_playlist(file_name):
    '''
    get_playlist permet d'utiliser le server afin de générer une playlist aléatoire
    à l'aide d'un fichier json contenant des noms d'artistes.
    La fonction s'aide du package random  et de sa méthode random.choices 
    afin de choisir aléatoirement l'occurence de la présence des artistes dans la playlist. 


    Arguments :
        file_name (str) : Nom du fichier json contenant la liste d'artistes préférés

    Retour :
        playlist (list) : Liste de dictionnaire contenant des informatiosn diverses sur chaque musique générés aléatoirement.
    '''
    # Récupère les données dans la fichier json
    with open(file_name) as file:
        list_artist = json.load(file)

    # Crée une liste contenant le nom des artistes présent dans le fichier et une liste contenant la note qui leur a été attribué
    list_weight = []
    list_name = []
    for data in list_artist:
        list_weight.append(data["note"])
        list_name.append(data["artiste"])
        # La note attribué à chaque artiste sert de poids pour le choix aléatoire de l'occurence d'un artiste dans la playlist
        artist_list = random.choices(list_name, weights=list_weight, k=20)

    # Création de la playlist sous la forme d'une liste de dictionnaire, où chaque dictionnaire représente des informations diverses sur la musique
    playlist = []
    for artist in artist_list:
        config = dotenv_values("../.env")
        r = requests.get(config["URL"] + "/random/" + artist)
        playlist.append(r.json())

    return(playlist)


def write_playlist(playlist):
    '''
    La fonction write_playlist permet d'écrire la playlist dans un fichier json qui se retrouvera dans le dossier client.

    Arguments :
        playlist (list) :  Liste de dictionnaire contenant des informatiosn diverses sur chaque musique générés aléatoirement.
    '''
    # Ecriture de la liste 'playlist' dans un fichier json afin que cela soit plus facile à lire que dans le terminal
    with open('playlist.json', 'w') as playlist_file:
        json.dump(playlist, playlist_file, indent=4)


if __name__ == "__main__":
    load_dotenv()
    playlist = get_playlist(os.getenv("file_name"))
    write_playlist(playlist)
