import requests
import random


def get_idartiste(artiste_name: str):
    '''
    get_idartiste permet de récupérer l'identifiant de l'artiste en lien avec l'API de theaudiodb. Grâce à cet identifiant,
    il sera plus facile de récupérer d'autre informations sur cet artiste avec l'API.

    Arguments :
        artiste_name (str) : Nom de l'artiste

    Retour : 
        id_artist (int) : Identifiant de l'artiste
    '''
    url = "https://www.theaudiodb.com/api/v1/json/2/search.php?s=" + \
        str(artiste_name)
    r = requests.get(url)
    id_artist = r.json()['artists'][0]['idArtist']
    return(id_artist)


def get_idalbum(artiste_name: str):
    '''
    get_idalbum permet de récupérer l'identifiant d'un album au hasard d'un artiste. Pour ce faire, la fonction utilise 
    l'API theaduiodb et la fonction get_idartiste.

    Arguments :
        artiste_name (str) : Nom de l'artiste pour lequel on veut un album au hasard

    Retour :
        id_album (int) : Identifiant de l'album tiré au hasard.
    '''
    idartiste = get_idartiste(artiste_name)
    url = "https://theaudiodb.com/api/v1/json/2/album.php?i=" + \
        str(idartiste)
    r = requests.get(url)
    liste_album = r.json()["album"]
    random_id = random.randint(0, len(liste_album)-1)
    id_album = liste_album[random_id]["idAlbum"]
    return(id_album)


def get_random_song(artiste_name: str):
    '''
    get_random_song permet de récupérer une musique au hasard d'un album d'un artiste ainsi que le lien vers son clip youtube. Pour ce faire, la fonction utilise
    l'API the audiodb et la fonction get_idalbum.

    Arguments :
        artiste_name (str) : Nom de l'artiste pour lequel on veut une musique au hasard.

    Retour :
        elements (list) : Liste contenant en premier élément le nom de la musique ainsi qu'en second élément l'url vers le clip Youtube.
    '''
    id_album = get_idalbum(artiste_name)
    url = "https://theaudiodb.com/api/v1/json/2/track.php?m=" + str(id_album)
    r = requests.get(url)
    liste_song = r.json()["track"]
    random_id = random.randint(0, len(liste_song)-1)

    elements = [liste_song[random_id]["strTrack"],
                liste_song[random_id]["strMusicVid"]]
    return(elements)


def get_lyrics(artist_name: str, artist_song: str):
    '''
    get_lyrics permet de récupérer les lyrics d'une musique. Pour ce faire la fonction utilise l'API lyrics.ovh.

    Arguments :
        artiste_name (str) : Nom de l'artiste
        artist_song (str) : Nom de la musique pour laquelle on veut récupérer les lyrics

    Retour :
        lyrics (str) : Chaîne de caractère contenant le début des lyrics s'ils existent. 
    '''
    url = "https://api.lyrics.ovh/v1/" + artist_name + "/" + artist_song
    r = requests.get(url)
    lyrics_json = r.json()
    if not('lyrics' in lyrics_json.keys()):
        lyrics = "no lyrics"
    else:
        lyrics = lyrics_json['lyrics'][:50] + "..."
    return(lyrics)
