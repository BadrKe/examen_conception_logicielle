import requests


def healthcheck_lyrics():
    request = requests.get(
        'https://api.lyrics.ovh/v1/Coldplay/Adventure of a Lifetime')
    test = request.status_code
    return test


def healthcheck_audioDB():
    request = requests.get(
        'https://www.theaudiodb.com/api/v1/json/2/search.php?s=coldplay')
    test = request.status_code
    return test
