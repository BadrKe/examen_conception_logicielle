from fastapi import FastAPI
from get_artiste import get_random_song
from get_artiste import get_lyrics
import uvicorn

from health_check import healthcheck_lyrics
from health_check import healthcheck_audioDB

app = FastAPI()


@app.get("/")
def read_root():
    test_lyrics = healthcheck_lyrics()
    test_audioDB = healthcheck_audioDB()
    return{'Status lyrics.ovh': test_lyrics, 'Status audioDB': test_audioDB}


@app.get("/random/{artist_name}")
def get_song(artist_name: str):
    '''
    get_song permet de récupérer diverses informations sur une musique aléatoire d'un artiste choisit.

    Arguments :
        artiste_name (str) : Nom de l'artiste choisit

    Retour :
        result (dic) : Dictionnaire contenant le nom de l'artiste, une de ses musiques, l'URL vers le clip youtube et les lyrics.
    '''
    random_song = get_random_song(artist_name)
    lyrics = get_lyrics(artist_name, random_song[0])
    result = {"artist": artist_name,
              "track": random_song[0], "suggested_youtube_url": random_song[1],
              "lyrics": lyrics}
    return(result)


if __name__ == "__main__":
    uvicorn.run(app)
