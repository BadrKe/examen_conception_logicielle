# Generateur de Playlist

Cette application permet de générer une playlist à partir d'un fichier json contenant le nom de vos artiste préféré ainsi que d'une note (sur 20) que vous lui attribuez. Cette note permet de lui donner plus ou moins d'importance dans la playlist générée.

## Schéma d'architecture

```mermaid
graph LR
    A(Client)
    A --> B{Server}
    B --> C(lyrics.ovh API)
    B --> D(AudioDB API)
```


## Avant toute chose
Avant de vouloir utiliser l'application, veillez à bien installer les prérequis :
````
git clone https://gitlab.com/BadrKe/examen_conception_logicielle.git
cd examen_conception_logicielle
pip install -r requirements.txt
````

## Lancer le serveur
Pour pouvoir générer une playlist et donc utiliser le client, il faut d'abord que le serveur soit lancé. 
Pour ce faire, tapez dans un terminal : 

````
cd server
python3 main.py
````

Vérifiez bien que l'URL affiché dans votre terminal soit la même que celle renseigné dans le fichier .env . Si ce n'est pas le cas, veuillez renseigner le bon URL dans le .env afin de pouvoir utiliser le client. 

## Generer une playlist 
Pour génerer une playlist selon vos artistes préférés, veuillez renseigner un fichier json au même format que celui présent de base "rudy.json". Il doit contenir une liste de dictionnaires à deux clés ("artiste" et "note").
Veuillez ensuite renseigner le nom de ce fichier json dans le .env à la place de "rudy.json"
Puis, ouvrez un nouveau terminal (en ayant toujours le serveur du terminal allumé) et tapez :
```
cd client
python3 main.py
```
Au bout de quelque instants, un fichier json nommé playlist est crée dans le répertoire client. Il contient la playlist générée avec le titres des artistes et des chansons. Lorsque cela est possible, il y a aussi le début des lyrics de la chanson et un lien vers le clip youtube.

## Test unitaire

Pour vérifier si l'application fonctionne bien, vous pouvez lancer un test unitaire qui va vérifier si la playlist généré contient bien les artistes désirés. (Ceux mentionné dans le fichier json test.json)

```
cd client
python3 -m unittest unitest/test_playlist.py
```